// $Id: wysiwyg.init.js,v 1.1.2.4 2008/11/30 17:16:42 sun Exp $

Drupal.behaviors = Drupal.behaviors || {}; // D5 only.

Drupal.wysiwyg = Drupal.wysiwyg || { 'instances': {} };

Drupal.wysiwyg.editor = Drupal.wysiwyg.editor || { 'init': {}, 'attach': {}, 'detach': {} };

Drupal.wysiwyg.plugins = Drupal.wysiwyg.plugins || {};

